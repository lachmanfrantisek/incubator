For the secret files, it is safer (and more usefull) to use a build-time volume. You can mount it like the volume to the container and the content can be reached only during the build.

There is a small example created for you.

## Source directory

In the terminal, we can see a content of the directory `/secret_volume`. There are two files:

* `secret_key`, that has "My secret key!!!" as a content
* `another_secret_file`

## Dockerfile

The content of the Dockerfile was changed. You can see it in the terminal or here:

```
FROM fedora:25
RUN cat /secret/secret_key
```

## Build

During the build you have to specify the `-v`/`--volume` option:

```
incubator build -t mysecretimage \
		-v /secret_volume:/secret \
		--verbose
```

## Final image

In the output, we can see the content of the file from the secret volume:

```
:
:

----------> RUN cat /secret/secret_key

My secret key!!!

:
:
```


When we run the container from the image, the folder is empty:

`docker run mysecretimage ls -l /secret`


> The secret directory is only mounted during build. The files are not present in the result. (They are also not hidden in the lower image layers like when you `COPY` your files into image and remove them in the another line.)
