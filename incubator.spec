# Created by pyp2rpm-3.2.1
%global pypi_name incubator
%global _summary Python library for building container images

Name:           python-%{pypi_name}
Version:        0.5.0
Release:        2%{?dist}
Summary:        %{_summary}

License:        MIT
URL:            https://gitlab.com/lachmanfrantisek/incubator
Source0:        https://files.pythonhosted.org/packages/source/i/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-tox
BuildRequires:  python3-setuptools
BuildRequires:  python3-dockerfile-parse >= 0.0.6
BuildRequires:  python3-jsonschema
BuildRequires:  python3-click
BuildRequires:  python3-docker

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-tox
BuildRequires:  python2-setuptools
BuildRequires:  python2-dockerfile-parse >= 0.0.6
BuildRequires:  python2-jsonschema
BuildRequires:  python2-click
BuildRequires:  python2-docker

%description
Alternative image builder for docker containers. Provides python2/python3 api
and cli tool incubator. Extends the functionality of basic docker build with
precise layering, ability to have build time volumes and some other useful
options, that can be configured with a python dictionary or a json file.

%package -n     python3-%{pypi_name}
Summary:        %{_summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-dockerfile-parse >= 0.0.6
Requires:       python3-jsonschema
Requires:       python3-click
Requires:       python3-docker
%description -n python3-%{pypi_name}
Alternative image builder for docker containers. Provides python2/python3 api
and cli tool incubator.Extends the functionality of basic docker build with
precise layering, ability to have build time volumes and some other useful
options, that can be given as a python dictionary or json file.

%package -n     python2-%{pypi_name}
Summary:        %{_summary}
%{?python_provide:%python_provide python2-%{pypi_name}}

Requires:       python2-dockerfile-parse >= 0.0.6
Requires:       python2-jsonschema
Requires:       python2-click
Requires:       python2-docker
%description -n python2-%{pypi_name}
Alternative image builder for docker containers. Provides python2/python3 api
and cli tool incubator.Extends the functionality of basic docker build with
precise layering, ability to have build time volumes and some other useful
options, that can be given as a python dictionary or json file.


%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build
%py2_build

%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py2_install
cp %{buildroot}/%{_bindir}/incubator %{buildroot}/%{_bindir}/incubator-2
ln -sf %{_bindir}/incubator-2 %{buildroot}/%{_bindir}/incubator-%{python2_version}

%py3_install
cp %{buildroot}/%{_bindir}/incubator %{buildroot}/%{_bindir}/incubator-3
ln -sf %{_bindir}/incubator-3 %{buildroot}/%{_bindir}/incubator-%{python3_version}


%check
py.test-%{python2_version} -vv tests || :
py.test-%{python3_version} -vv tests || :

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{_bindir}/incubator
%{_bindir}/incubator-3
%{_bindir}/incubator-%{python3_version}
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python2-%{pypi_name}
%license LICENSE
%doc README.md
%{_bindir}/incubator-2
%{_bindir}/incubator-%{python2_version}
%{python2_sitelib}/%{pypi_name}
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Fri Apr 28 2017 Tomas Tomecek <ttomecek@redhat.com> - 0.5.0-2
- Complete RPM packaging.

* Wed Apr 26 2017 root - 0.5.0-1
- Initial package.
