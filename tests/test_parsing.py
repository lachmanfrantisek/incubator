import unittest

import six

from incubator.core.parsing import apply_shell_expansion as apply_se
from incubator.core.parsing import (create_dockerfile_structure, quote,
                                    substitute_variables)


class TestParsing(unittest.TestCase):
    def test_apply_shell_expansion(self):
        for quotation_mark in ["'", '"']:
            self.assertEqual(apply_se('{0}string with {0}space'.format(quotation_mark)),
                             'string with space')

            self.assertEqual(apply_se('escaped \\{}'.format(quotation_mark)),
                             'escaped {}'.format(quotation_mark))

        self.assertEqual(apply_se('"\'"'),
                         "'")
        self.assertEqual(apply_se("'\"'"),
                         '"')

    def test_quote(self):
        self.assertEqual(quote('word'), 'word')
        self.assertEqual(quote('string with space'), "'string with space'")
        self.assertIn(quote("'"), ["''\"'\"''", "\"'\""])

    def test_quote_and_escape(self):
        for text in ["word", "text with space",
                     "text\" with \' quotes and spaces",
                     "'", "'", "\\'", '\\"',
                     "text with $ @ # $ * & and other special characters "
                     "like < > ? ! % ^ { } \\ , . - | / "]:
            self.assertEqual(apply_se(quote(text)), text)

    def test_substitute_variables(self):
        for val, env, result in [("without variables", {}, "without variables"),
                                 ("with $variable", {}, "with "),
                                 ("with $var", {"var": "variable"}, "with variable"),
                                 ("with ${var}.", {"var": "variable"}, "with variable."),
                                 ("$who likes ${what}.", {"who": "Pete", "what": "pizza"}, "Pete likes pizza."),
                                 ("$who likes ${what}.", {"who": "Pete"}, "Pete likes ."),
                                 ("$VAR", {}, ""),
                                 ("$VAR", {"VAR": "var"}, "var"),
                                 ("$VAR", {"var": "var"}, ""),
                                 ("", {}, ""),
                                 ("\\$VAR", {}, "$VAR"),
                                 ("\\$VAR", {"VAR": "var"}, "$VAR"),
                                 ("${VAR}", {"VAR": "var"}, "var"),
                                 ("${VAR_IABLE}", {"VAR_IABLE": "var"}, "var"),
                                 ]:
            self.assertEqual(substitute_variables(s=val, variables=env),
                             result)

    def test_substitute_variables_default_values(self):
        for val, env, result in [("${VAR:-default}", {"VAR": "var"}, "var"),
                                 ("${VAR:-default}", {}, "default"),
                                 ("${user:-some_user}", {}, "some_user"),
                                 ("${user:-some_user}", {"user": "name"}, "name"),
                                 ("{user:-some_user}", {}, "{user:-some_user}"),
                                 ("${user:-some-user}", {}, "some-user")
                                 ]:
            self.assertEqual(substitute_variables(s=val, variables=env),
                             result)

    def test_create_dockerfile_structure(self):
        content = "FROM fedora:25\n" \
                  "ENV key=value another_key=\"another value\" \n" \
                  "LABEL label=\"value of the label\" \n"
        df = six.BytesIO(content.encode())
        cmnds, env_label, base = create_dockerfile_structure(dockerfile_content=df)
        self.assertEqual(cmnds[0]['instruction'], "FROM")
        self.assertEqual(cmnds[0]['value'], "fedora:25")
        self.assertEqual(cmnds[1]['instruction'], "ENV")
        self.assertEqual(cmnds[1]['value'], "key=value another_key=\"another value\"")
        self.assertEqual(cmnds[2]['instruction'], "LABEL")
        self.assertEqual(cmnds[2]['value'], "label=\"value of the label\"")

        self.assertEqual(env_label[0].envs, {})
        self.assertEqual(env_label[0].labels, {})
        self.assertEqual(env_label[0].line_envs, {})
        self.assertEqual(env_label[0].line_labels, {})

        self.assertEqual(env_label[1].envs, {"key": "value",
                                             "another_key": "another value"})
        self.assertEqual(env_label[1].labels, {})
        self.assertEqual(env_label[1].line_envs, {"key": "value",
                                                  "another_key": "another value"})
        self.assertEqual(env_label[1].line_labels, {})

        self.assertEqual(env_label[2].envs, {"key": "value",
                                             "another_key": "another value"})
        self.assertEqual(env_label[2].labels, {"label": "value of the label"})
        self.assertEqual(env_label[2].line_envs, {})
        self.assertEqual(env_label[2].line_labels, {"label": "value of the label"})

        self.assertEqual(base, "fedora:25")


if __name__ == '__main__':
    unittest.main()
