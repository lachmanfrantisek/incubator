import unittest

import click
import click.testing

import incubator
from incubator.cli.incubator import cli


class TestCLI(unittest.TestCase):
    def setUp(self):
        self.runner = click.testing.CliRunner()

    def test_option_version(self):
        for option in ["-V", "--version"]:
            result = self.runner.invoke(cli, [option])
            self.assertEqual(result.exit_code, 0)
            self.assertEqual(result.output, "{}-{}\n".format("incubator", incubator.__version__))

    def test_squash(self):
        result = self.runner.invoke(cli, ["build", "--squash", "--test-config"])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("split_layer: [0]", result.output)
        self.assertIn("squash: True", result.output)
        self.assertIn("default_layering: False", result.output)

    def test_default_layering(self):
        result = self.runner.invoke(cli, ["build", "--default-layering", "--test-config"])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("split_layer: []", result.output)
        self.assertIn("squash: False", result.output)
        self.assertIn("default_layering: True", result.output)

    def test_squash_and_default_layering(self):
        result = self.runner.invoke(cli, ["build", "--squash", "--default-layering", "--test-config"])
        self.assertEqual(result.exit_code, 2)

        result = self.runner.invoke(cli, ["build", "--default-layering", "--squash", "--test-config"])
        self.assertEqual(result.exit_code, 2)

        result = self.runner.invoke(cli, ["build", "-s 2", "--squash", "--default-layering", "--test-config"])
        self.assertEqual(result.exit_code, 2)

    def test_layering(self):
        for options, result_output in [(["-s 2"], "[2]"),
                                       (["-s 2", "-s 5"], "[2, 5]"),
                                       (["-s 0"], "[0]")]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn("split_layer: {}".format(result_output), result.output)

        for options in [["-s -1"], ["-s 2", "-s -5"]]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 2)

    def test_rm(self):
        result = self.runner.invoke(cli, ["build", "--test-config", "."])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("rm: None", result.output)

        for options in [["--rm", "True"], ["--rm", "true"]]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn("rm: True", result.output)

    def test_volume(self):
        for options, result_output in [(['-v', 'source:destination'], "['source:destination']"),
                                       (['-v', 'source:destination:ro'], "['source:destination:ro']"),
                                       (['-v', 's1:d1:ro', "-v", 's2:d2:z'], "['s1:d1:ro', 's2:d2:z']"),
                                       (['-v', 'source:d1', "-v", 'source:d2'], "['source:d1', 'source:d2']")]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn("volumes: {}".format(result_output), result.output)

    def test_path(self):
        for option, result_output in [(".", "."),
                                      ("/tmp", "/tmp")]:
            result = self.runner.invoke(cli, ["build", "--test-config", option])
            self.assertEqual(result.exit_code, 0)
            self.assertIn("path: {}".format(result_output), result.output)

        result = self.runner.invoke(cli, ["build", "--test-config"])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("path: .", result.output)

        result = self.runner.invoke(cli, ["build", "--test-config", "non/existing/path"])
        self.assertEqual(result.exit_code, 2)

    def test_build_arg(self):
        for options, result_output in [(["--build-arg", "A=B"], "{'A': 'B'}"),
                                       (["--build-arg", "A=B", "--build-arg", "A=C"], "{'A': 'C'}"),
                                       (["--build-arg", "A=bbb bbb"], "{'A': 'bbb bbb'}")]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn("buildargs: {}".format(result_output), result.output)

            result = self.runner.invoke(cli, ["build", "--test-config", "--build-arg", "A=B", "--build-arg", "c=d"])
            self.assertEqual(result.exit_code, 0)
            self.assertTrue(
                "buildargs: {'A': 'B', 'c': 'd'}" in result.output or "buildargs: {'c': 'd', 'A': 'B'}" in result.output)

            result = self.runner.invoke(cli, ["build", "--build-arg"])
            self.assertEqual(result.exit_code, 2)

            result = self.runner.invoke(cli, ["build", "--build-arg", "a:b"])
            self.assertEqual(result.exit_code, 2)

            result = self.runner.invoke(cli, ["build", "--build-arg", "a b"])
            self.assertEqual(result.exit_code, 2)

    def test_limits(self):
        for options, result_output in [(["--memory", "512MB"], "memory: 512MB"),
                                       (["-m", "1024"], "memory: 1024"),
                                       (["--memory-swap", "2048"], "memory_swap: 2048"),
                                       (["--memory-swap", "-1"], "memory_swap: -1"),
                                       (["--cpu-shares", "80"], "cpu_shares: 80"),
                                       (["-c", "5"], "cpu_shares: 5"),
                                       (["--cpuset-cpus", "5"], "cpuset_cpus: 5"),
                                       (["--cpuset-cpus", "0-3"], "cpuset_cpus: 0-3"),
                                       (["--cpuset-cpus", "1,2"], "cpuset_cpus: 1,2")]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn(result_output, result.output)

        result = self.runner.invoke(cli, ["build", "--cpu-shares", "-1"])
        self.assertEqual(result.exit_code, 2)

        result = self.runner.invoke(cli, ["build", "--cpu-shares", "five"])
        self.assertEqual(result.exit_code, 2)

    def test_context_file_limit(self):
        for options, result_output in [(["--context-file-limit", "0"], "context_file_limit: 0"),
                                       (["--context-file-limit", "512"], "context_file_limit: 512")]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn(result_output, result.output)

        result = self.runner.invoke(cli, ["build", "--context-file-limit", "-1"])
        self.assertEqual(result.exit_code, 2)

        result = self.runner.invoke(cli, ["build", "--context-file-limit", "five"])
        self.assertEqual(result.exit_code, 2)

    def test_force_rm(self):
        result = self.runner.invoke(cli, ["build", "--test-config", "--force-rm"])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("force_rm: True", result.output)

        result = self.runner.invoke(cli, ["build", "--test-config"])
        self.assertEqual(result.exit_code, 0)
        self.assertIn("force_rm: None", result.output)

    def test_build_labels(self):
        for options, result_output in [(["--label", "A=B"], "{'A': 'B'}"),
                                       (["-l", "A=B"], "{'A': 'B'}"),
                                       (["-l", "A=B", "-l", "A=C"], "{'A': 'C'}"),
                                       (["-l", "A=bbb bbb"], "{'A': 'bbb bbb'}")]:
            result = self.runner.invoke(cli, ["build", "--test-config"] + options)
            self.assertEqual(result.exit_code, 0)
            self.assertIn("labels: {}".format(result_output), result.output)

        result = self.runner.invoke(cli, ["build", "--test-config", "--label", "A=B", "-l", "c=d"])
        self.assertEqual(result.exit_code, 0)
        self.assertTrue(
            "labels: {'A': 'B', 'c': 'd'}" in result.output or "labels: {'c': 'd', 'A': 'B'}" in result.output)

        result = self.runner.invoke(cli, ["build", "--labels", "A=B"])
        self.assertEqual(result.exit_code, 2)

        result = self.runner.invoke(cli, ["build", "--label", "a:b"])
        self.assertEqual(result.exit_code, 2)

        result = self.runner.invoke(cli, ["build", "-l", "a b"])
        self.assertEqual(result.exit_code, 2)


if __name__ == '__main__':
    unittest.main()
