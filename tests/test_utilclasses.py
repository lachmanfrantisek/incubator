import unittest

from incubator.core.constants import VOLUME_MODES
from incubator.core.utilclasses import Volume


class TestUtilClasses(unittest.TestCase):
    def test_volume_nonvalid(self):
        self.assertRaises(ValueError, Volume.get_instance, None)
        self.assertRaises(ValueError, Volume.get_instance, "abcde")
        self.assertRaises(ValueError, Volume.get_instance, "a:b:c:d")

    def test_volume_nonvalid_mode(self):
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:r")
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:row")
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:cow")
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:a")
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:read")
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:write")
        self.assertRaises(ValueError, Volume.get_instance, "source:destination:read-only")

    def test_volume_valid(self):
        v1 = Volume.get_instance("a/b/c:d/e/f")
        self.assertEqual(v1.source, "a/b/c")
        self.assertEqual(v1.destination, "d/e/f")
        self.assertIsNone(v1.mode)

        v2 = Volume.get_instance("a/b/c:/d/ef/g:ro")
        self.assertEqual(v2.source, "a/b/c")
        self.assertEqual(v2.destination, "/d/ef/g")
        self.assertEqual(v2.mode, "ro")

        for mode in VOLUME_MODES:
            v2 = Volume.get_instance("source:destination:{}".format(mode))
            self.assertEqual(v2.mode, mode)

    def test_volume_equal(self):
        v1 = Volume.get_instance("a/b/c:d/e/f")
        v2 = Volume.get_instance("a/b/c:d/e/f")
        self.assertTrue(v1 == v2)
        self.assertTrue(v2 == v1)

        self.assertFalse(v1 == Volume.get_instance("a/b/c:d/e/f:ro"))
        self.assertFalse(v1 == Volume.get_instance("a/b/c:/d/e/f"))

        v3 = Volume.get_instance("a:b")
        v3.source = "a/b/c"
        v3.destination = "d/e/f"
        self.assertTrue(v1 == v3)

        v4 = Volume.get_instance("a/b/c:d/e/f:rw")
        v5 = Volume.get_instance("a/b/c:d/e/f:rw")
        self.assertTrue(v4 == v5)
        self.assertTrue(v5 == v4)
        self.assertFalse(v4 == Volume.get_instance("a/b/c:d/e/f:ro"))

    def test_volume_eq(self):
        v1 = Volume.get_instance("a/b/c:d/e/f")
        v2 = Volume.get_instance("a/b/c:d/e/f")
        self.assertEqual(v1.__hash__(), v2.__hash__())

        self.assertNotEqual(v1.__hash__(),
                            Volume.get_instance("a/b/c:d/e/f:ro").__hash__())
        self.assertNotEqual(v1.__hash__(),
                            Volume.get_instance("a/b/c:/d/e/f").__hash__())

        v3 = Volume.get_instance("a:b")
        v3.source = "a/b/c"
        v3.destination = "d/e/f"
        self.assertEqual(v1.__hash__(), v3.__hash__())

        v4 = Volume.get_instance("a/b/c:d/e/f:rw")
        v5 = Volume.get_instance("a/b/c:d/e/f:rw")
        self.assertEqual(v4.__hash__(), v5.__hash__())
        self.assertEqual(v5.__hash__(), v4.__hash__())
        self.assertNotEqual(v4.__hash__(),
                            Volume.get_instance("a/b/c:d/e/f:ro").__hash__())

    def test_volume_get_instances(self):
        self.assertEqual(Volume.get_instances(None), None)
        self.assertEqual(Volume.get_instances(["a:b"]),
                         [Volume(source="a",
                                 destination="b")])

        self.assertEqual(Volume.get_instances(["a:b", "c:d:ro"]),
                         [Volume(source="a",
                                 destination="b"),
                          Volume(source="c",
                                 destination="d",
                                 mode="ro")])

        self.assertRaises(ValueError, Volume.get_instances, ["a:b:c:d"])

    def test_volumes_to_list(self):
        v1 = Volume.get_instance("a/b/c:d/e/f:rw")
        v2 = Volume.get_instance("a/b/c:/ij/k:ro")
        v3 = Volume.get_instance("a/b/c:d/e/f")

        self.assertEqual(Volume.volumes_to_list([v1, v2, v3]),
                         ["a/b/c:d/e/f:rw", "a/b/c:/ij/k:ro", "a/b/c:d/e/f"])

        self.assertEqual(Volume.volumes_to_list([]), [])

    def test_volumes_to_bind_dict(self):
        v1 = Volume.get_instance("a/b/c1:d/e/f:rw")
        v2 = Volume.get_instance("a/b/c2:/ij/k:ro")
        v3 = Volume.get_instance("a/b/c3:d/e/f")

        self.assertEqual(Volume.volumes_to_bind_dict([v1, v2, v3]), {
            "a/b/c1": {"bind": "d/e/f", "mode": "rw"},
            "a/b/c2": {"bind": "/ij/k", "mode": "ro"},
            "a/b/c3": {"bind": "d/e/f", "mode": None}
        })

        self.assertEqual(Volume.volumes_to_bind_dict([]), {})


if __name__ == '__main__':
    unittest.main()
